require 'json'

run lambda { |env| [200, {Rack::CONTENT_TYPE => 'application/json'}, [{'now' => Time.now}.to_json]] }
